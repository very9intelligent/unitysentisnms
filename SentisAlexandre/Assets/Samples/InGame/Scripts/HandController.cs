using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandController : MonoBehaviour
{

    float initialZ = -5f;
    GameObject draggingObject;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        // Update position
        transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, -Camera.main.transform.position.z));
        transform.position = new Vector3(transform.position.x, transform.position.y, initialZ);
        if (draggingObject != null)
            draggingObject.transform.position = new Vector3(transform.position.x, transform.position.y, draggingObject.transform.position.z);

        // Define the size of the box (width and height)
        Vector2 size = new Vector2(1, 1);

        // Get the center of the box (position of your 2D object)
        Vector2 center = transform.position;

        // Check for all colliders overlapping the box
        Collider2D[] colliders = Physics2D.OverlapBoxAll(center, size, 0f);

        // Iterate through all overlapping colliders
        foreach (Collider2D collider in colliders)
        {
            // Do something with the overlapping collider
            if (collider.gameObject != gameObject)
            {
                if (collider.gameObject.tag == "box" && (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.Mouse0)))
                    draggingObject = collider.gameObject;
                else if (draggingObject != null && !(Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.Mouse0)))
                {
                    draggingObject.GetComponent<BagItem>().OnDrop();
                    draggingObject = null;
                }
                    
            }
               // Debug.Log("Overlapping object: " + collider.name);
        }
    }
    /*
    private void OnCollisionStay2D(Collision2D collision)
    {
        Debug.Log(collision.gameObject.name);
        // No caso usar o z e procurar e verificar a interface
        
        else if(draggingObject != null && !(Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.Mouse0)))
            draggingObject = null;
    }*/

}
