using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BagItem : MonoBehaviour
{

    GameObject parent;

    // Start is called before the first frame update
    void Start()
    {
        parent = transform.parent.gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void OnDrop()
    {
        // Define the size of the box (width and height)
        Vector2 size = new Vector2(1, 1);

        // Get the center of the box (position of your 2D object)
        Vector2 center = transform.position;

        // Check for all colliders overlapping the box
        Collider2D[] colliders = Physics2D.OverlapBoxAll(center, size, 0f);

        // Iterate through all overlapping colliders
        foreach (Collider2D collider in colliders)
        {
            // Do something with the overlapping collider
            if (collider.gameObject != gameObject)
            {
                if (collider.gameObject.tag == "boxzone")
                {
                    parent = collider.gameObject;
                    transform.parent = collider.gameObject.transform;
                    break;
                }
                    
            }
        }
        transform.position = parent.transform.position;
    }

    /*
    private void OnCollisionStay2D(Collision2D collision)
    {
        Debug.Log(collision.gameObject.name);
        // No caso usar o z e procurar e verificar a interface
        if (collision.gameObject.tag == "boxzone" && (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.Mouse0)))
            draggingObject = collision.gameObject;
        else if (draggingObject != null && !(Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.Mouse0)))
            draggingObject = null;
    }*/

}
