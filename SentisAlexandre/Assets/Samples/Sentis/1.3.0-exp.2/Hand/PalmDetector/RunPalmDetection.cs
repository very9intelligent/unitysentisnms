using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Sentis;
using UnityEngine.Video;
using UnityEngine.UI;
using Lays = Unity.Sentis.Layers;

/*
 *                   Palm Detection Inference
 *                   ========================
 *                   
 * Basic inference script for mediapose palm detection
 * 
 * Put this script on the Main Camera
 * Put palm_detection_full_inf_post_192x192.sentis in the Assets/StreamingAssets folder
 * Create a RawImage of in the scene
 * Put a link to that image in previewUI
 * Put a video in Assets/StreamingAssets folder and put the name of it int videoName
 * Or put a test image in inputImage
 * Set inputType to appropriate input
 */
[RequireComponent(typeof(ai.hand.HandCollection))]
public class RunPalmDetection : ai.hand.PalmExtractor
{
    //Size of input image to neural network (196)
    const int size = 192;

    #region starting
    // Start is called before the first frame update
    void Start()
    {
        

        //(Note: if using a webcam on mobile get permissions here first)
        SetupTextures();
        SetupMarkers(Color.red);
        SetupInput();
        SetupModel();
        SetupEngine();

        // Create palm tracker
        handlandmark = GetComponent<RunHandLandmark>();
        handCollection = GetComponent<ai.hand.HandCollection>();
        handTracking = GetComponent<ai.hand.HandTracking>();
        handPoseEstimation = GetComponent<ai.hand.HandPoseEstimation>();
        unityServer = GetComponent<ai.server.UnityHandServer>();
    }

    public void SetupEngine()
    {
        worker = WorkerFactory.CreateWorker(backend, model);
        ops = WorkerFactory.CreateOps(backend);
    }

    void SetupModel()
    {
        model = ModelLoader.Load(Application.streamingAssetsPath + "/palm_detection_full_inf_post_192x192.sentis");
        //model = ModelLoader.Load(modelAsset);

    }

    void SetupMarkers(Color color)
    {
        markerPixels = new Color32[markerWidth * markerWidth];
        for (int n = 0; n < markerWidth * markerWidth; n++)
        {
            markerPixels[n] = color;
        }
        int center = markerWidth / 2;
        markerPixels[center * markerWidth + center] = Color.black;
    }

    void SetupInput()
    {
        switch (inputType)
        {
            case InputType.Webcam:
                {
                    webcam = new WebCamTexture(deviceName, resolution.x, resolution.y);
                    webcam.requestedFPS = 30;
                    webcam.Play();
                    break;
                }
            case InputType.Video:
                {
                    video = gameObject.AddComponent<VideoPlayer>();//new VideoPlayer();
                    video.renderMode = VideoRenderMode.APIOnly;
                    video.source = VideoSource.Url;
                    video.url = Application.streamingAssetsPath + "/" + videoName;
                    video.isLooping = true;
                    video.Play();
                    break;
                }
            default:
                {
                    Graphics.Blit(inputImage, targetTexture);
                }
                break;
        }
    }

    void SetupTextures()
    {
        //To display the get and display the original image:
        targetTexture = new RenderTexture(resolution.x, resolution.y, 0);

        //Used for drawing the markers:
        canvasTexture = new Texture2D(targetTexture.width, targetTexture.height);
        if(previewUI != null && printInPreview)
            previewUI.texture = targetTexture;
    }

    #endregion

    #region updating
    void GetImageFromSource()
    {
        if (inputType == InputType.Webcam)
        {
            // Format video input
            if (!webcam.didUpdateThisFrame) return;
            
            var aspect1 = (float)webcam.width / webcam.height;
            var aspect2 = (float)resolution.x / resolution.y;
            var gap = aspect2 / aspect1;

            var vflip = webcam.videoVerticallyMirrored;
            var scale = new Vector2(gap, vflip ? -1 : 1);
            var offset = new Vector2((1 - gap) / 2, vflip ? 1 : 0);

            Graphics.Blit(webcam, targetTexture, new Vector2(1,1), new Vector2(0,0));
        }
        if (inputType == InputType.Video)
        {
            var aspect1 = (float)video.width / video.height;
            var aspect2 = (float)resolution.x / resolution.y;
            var gap = aspect2 / aspect1;

            var vflip = false;
            var scale = new Vector2(gap, vflip ? -1 : 1);
            var offset = new Vector2((1 - gap) / 2, vflip ? 1 : 0);
            Graphics.Blit(video.texture, targetTexture, scale, offset);
        }
        if (inputType == InputType.Image)
        {
            Graphics.Blit(inputImage, targetTexture);
        }
    }

    // Update is called once per frame
    void Update()
    {

        GetImageFromSource();

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            closing = true;
            Application.Quit();
        }

        if (Input.GetKeyDown(KeyCode.P))
        {
            previewUI.enabled = !previewUI.enabled;
        }
        //Debug.Log(1 / (Time.deltaTime));
    }
    #endregion

    #region LateUpdating
    void LateUpdate()
    {
        if (!closing)
        {
            
            RunInference(targetTexture);
            if(handlandmark != null && handlandmark.enabled)
                handlandmark?.RunInference(this);
            if (handTracking != null && handTracking.enabled)
                handTracking.RunInference(handCollection);
            if (handPoseEstimation != null && handPoseEstimation.enabled)
                handPoseEstimation.RunInference(handCollection, handTracking);
            if (unityServer != null && unityServer.enabled)
                unityServer.UpdateServerResponse();
        }
    }

    void RunInference(Texture source)
    {
        var transform = new TextureTransform();
        transform.SetDimensions(size, size, 3);
        transform.SetTensorLayout(0, 1, 2, 3);
        using var image0 = TextureConverter.ToTensor(targetTexture, transform);
        // Pre-process the image to make input in range (-1..1)
        
        //using var imagetemp = ops.Mul(2f, image0);
        //using var image = ops.Add(imagetemp, -1f);
        worker.Execute(image0);

        using var landmarks = worker.PeekOutput("pdscore_boxx_boxy_boxsize_kp0x_kp0y_kp2x_kp2y") as TensorFloat;

        //using var landmarks = worker.PeekOutput("conv2d_21") as TensorFloat;

        //This gives the condifidence:
        //using var confidence = worker.PeekOutput("conv2d_31") as TensorFloat;

        float scaleX = targetTexture.width * 1f / size;
        float scaleY = targetTexture.height * 1f / size;

        landmarks.CompleteOperationsAndDownload();
        //Debug.Log(landmarks);
        DrawLandmarks(landmarks, scaleX, scaleY, source);
    }

    float NormalizeRadians(float angle)
    {
        return angle - 2 * Mathf.PI * Mathf.Floor((angle + Mathf.PI) / (2 * Mathf.PI));
    }

    void DrawLandmarks(TensorFloat landmarks, float scaleX, float scaleY, Texture source)
    {
        // palm_scr_threshold
        int numLandmarks = landmarks.shape[0]; //86 palm detection landmarks

        RenderTexture.active = targetTexture;
        canvasTexture.ReadPixels(new Rect(0, 0, targetTexture.width, targetTexture.height), 0, 0);
        int temp = 0;

        handCollection.handsOnScreen = new List<ai.hand.Hand>();
        for (int n = 0; n < numLandmarks; n++)
        {
            
            float pd_score = (float)(landmarks[n, 0]);
            float box_x = (landmarks[n, 1]);
            float box_y = (landmarks[n, 2]);
            float box_size = (float)(landmarks[n, 3]);
            float kp0_x = (float)(landmarks[n, 4] * scaleX);
            float kp0_y = (float)(landmarks[n, 5] * scaleY);
            float kp2_x = (float)(landmarks[n, 6] * scaleX);
            float kp2_y = (float)(landmarks[n, 7] * scaleY);

            
            if (pd_score > palm_scr_threshold && box_size > 0) {
                temp += 1;
                float kp02_x = kp2_x - kp0_x;
                float kp02_y = kp2_y - kp0_y;
                float sqn_rr_size = 2.5f * box_size;
                //Debug.Log("sqn : " + sqn_rr_size + ", box : " + box_size);
                float rotation = 0.5f * Mathf.PI - Mathf.Atan2(-kp02_y, kp02_x);
                rotation = NormalizeRadians(rotation);
                float sqn_rr_center_x = (box_x + 0.5f * box_size * Mathf.Sin(rotation));
                float sqn_rr_center_y = (box_y - 0.5f * box_size * Mathf.Cos(rotation));


                //Debug.Log("sqn_rr_size : "+ sqn_rr_size+ ", rotation : "+ rotation + ", sqn_rr_center_x : " + sqn_rr_center_x + ", sqn_rr_center_y : " + sqn_rr_center_y);                
                DrawPalmCenter(sqn_rr_center_x, sqn_rr_center_y, targetTexture.width, targetTexture.height, markerWidth);
                DrawPalmSquare(sqn_rr_size, rotation, sqn_rr_center_x, sqn_rr_center_y, targetTexture.width, targetTexture.height, markerWidth);
            }
        }
        
        if(printInPreview)
        {
            canvasTexture.Apply();
            Graphics.Blit(canvasTexture, targetTexture);
        }
            
        RenderTexture.active = null;
    }

    private void DrawPalmCenter(float sqn_rr_center_x, float sqn_rr_center_y, int width, int height,int markerWidth)
    {
        Vector2Int center = GetScreenPointFromPercentage(sqn_rr_center_x, sqn_rr_center_y, width, height, markerWidth);
        canvasTexture.SetPixels32(center.x, center.y, markerWidth, markerWidth, markerPixels);
    }


    private void DrawPalmSquare(float sqn_rr_size, float rotation, float sqn_rr_center_x, float sqn_rr_center_y,int width, int height,int markerWidth)
    {
        // Calculate pixels
        int cx = (int)(sqn_rr_center_x * width);
        int cy = (int)(sqn_rr_center_y * height);
        float wh_ratio = ((float)width) / height;
        int xmin = (int)((sqn_rr_center_x - (sqn_rr_size / 2)) * width);
        int xmax = (int)((sqn_rr_center_x + (sqn_rr_size / 2)) * width);
        int ymin = (int)((sqn_rr_center_y - (sqn_rr_size* wh_ratio / 2)) * height);
        int ymax = (int)((sqn_rr_center_y + (sqn_rr_size * wh_ratio / 2)) * height);
        xmin = Mathf.Max(xmin, 0);
        xmax = Mathf.Min(xmax, width);
        ymin = Mathf.Max(ymin, 0);
        ymax = Mathf.Min(ymax, height);
        float degree = rotation*(180f / Mathf.PI);
        // Creating a PalmSquare for this hand
        ai.hand.Hand palmSquare = new ai.hand.Hand();
        palmSquare.cornerleftydown = GetScreenPointFromPoint(xmin - (markerWidth / 2), ymin - (markerWidth / 2), width, height, markerWidth);
        palmSquare.cornerleftup = GetScreenPointFromPoint(xmin - (markerWidth / 2), ymax - (markerWidth / 2), width, height, markerWidth);
        palmSquare.cornerrightdown = GetScreenPointFromPoint(xmax - (markerWidth / 2), ymin, width - (markerWidth / 2), height, markerWidth);
        palmSquare.cornerrightup = GetScreenPointFromPoint(xmax - (markerWidth / 2), ymax - (markerWidth / 2), width, height, markerWidth);
        palmSquare.center = GetScreenPointFromPercentage(sqn_rr_center_x, sqn_rr_center_y, width, height, markerWidth);
        palmSquare.target_width = width;
        palmSquare.target_height = height;
        // Painting into canvas
        if (printInPreview)
        {
            canvasTexture.SetPixels32(palmSquare.cornerleftydown.x, palmSquare.cornerleftydown.y, markerWidth, markerWidth, markerPixels);
            canvasTexture.SetPixels32(palmSquare.cornerleftup.x, palmSquare.cornerleftup.y, markerWidth, markerWidth, markerPixels);
            canvasTexture.SetPixels32(palmSquare.cornerrightdown.x, palmSquare.cornerrightdown.y, markerWidth, markerWidth, markerPixels);
            canvasTexture.SetPixels32(palmSquare.cornerrightup.x, palmSquare.cornerrightup.y, markerWidth, markerWidth, markerPixels);
        }
        
        if (showlines && printInPreview) { 
            HandUtils.DrawLine(canvasTexture, palmSquare.cornerleftup.x, palmSquare.cornerrightdown.y+ (markerWidth / 2), palmSquare.cornerrightup.x, palmSquare.cornerrightdown.y + (markerWidth / 2), Color.red);
            HandUtils.DrawLine(canvasTexture, palmSquare.cornerleftup.x, palmSquare.cornerrightup.y + (markerWidth / 2), palmSquare.cornerrightup.x, palmSquare.cornerrightup.y + (markerWidth / 2), Color.red);
            HandUtils.DrawLine(canvasTexture, palmSquare.cornerleftup.x, palmSquare.cornerrightdown.y + (markerWidth / 2), palmSquare.cornerleftup.x, palmSquare.cornerrightup.y + (markerWidth / 2), Color.red);
            HandUtils.DrawLine(canvasTexture, palmSquare.cornerrightup.x, palmSquare.cornerrightdown.y + (markerWidth / 2), palmSquare.cornerrightup.x, palmSquare.cornerrightup.y + (markerWidth / 2)    , Color.red);
        }
        // Adding it to the list
        handCollection.handsOnScreen.Add(palmSquare);
    }



    private Vector2Int GetScreenPointFromPoint(int xpoint, int ypoint, int width, int height, int markerWidth)
    {
        int destX = Mathf.Clamp(xpoint - (markerWidth - 1) / 2, 0, width - 1 - markerWidth);
        int destY = Mathf.Clamp(height - 1 - (ypoint - (markerWidth - 1) / 2), 0, height - 1 - markerWidth);
        return new Vector2Int(destX, destY);
    }

    private Vector2Int GetScreenPointFromPercentage(float sqn_rr_center_x, float sqn_rr_center_y, int width, int height, int markerWidth)
    {
        int destX = Mathf.Clamp((int)(sqn_rr_center_x * width) - (markerWidth/2), 0, targetTexture.width - 1);
        int destY = Mathf.Clamp(targetTexture.height - 1 - (int)(sqn_rr_center_y * height) + (markerWidth / 2), 0, targetTexture.height - 1);
        return new Vector2Int(destX, destY);
    }


    #endregion


    void CleanUp()
    {
        closing = true;
        ops?.Dispose();
        if (webcam) Destroy(webcam);
        if (video) Destroy(video);
        RenderTexture.active = null;
        targetTexture.Release();
        worker?.Dispose();
        worker = null;
    }

    void OnDestroy()
    {
        CleanUp();
    }
}
