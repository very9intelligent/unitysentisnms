using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace ai.server
{
    [RequireComponent(typeof(ai.hand.HandPoseEstimation))]
    public class UnityHandServer : MonoBehaviour
    {
        private ai.hand.HandTracking handTracking;
        private ai.hand.HandPoseEstimation handPoseEstimation;
        public ai.server.ServerResponse lastResponse;

        // Start is called before the first frame update
        void Start()
        {
            handTracking = GetComponent<ai.hand.HandTracking>();
            handPoseEstimation = GetComponent<ai.hand.HandPoseEstimation>();
            lastResponse = new ServerResponse();
        }

        public void UpdateServerResponse()
        {
            lastResponse = new ServerResponse();
            ai.hand.Hand leftHand = null;
            ai.hand.Hand rightHand = null;
            if (handTracking.currentMainLeftHand != null)
                leftHand = handTracking.currentMainLeftHand.Clone();
            if(handTracking.currentMainRightHand != null)
                rightHand = handTracking.currentMainRightHand.Clone();

            lastResponse.left_hand_detected = (leftHand != null) ? 1 : 0;
            lastResponse.right_hand_detected = (rightHand != null) ? 1 : 0;
            
            if(lastResponse.left_hand_detected == 1)
            {
                lastResponse.left_hand_x = (leftHand.center.x *1f / leftHand.target_width);
                lastResponse.left_hand_y = (leftHand.center.y * 1f / leftHand.target_height);
                lastResponse.left_hand_pose = leftHand.handPose;
            }
            if(lastResponse.right_hand_detected == 1)
            {
                lastResponse.right_hand_x = (rightHand.center.x * 1f / rightHand.target_width);
                lastResponse.right_hand_y = (rightHand.center.y * 1f / rightHand.target_height);
                lastResponse.right_hand_pose = rightHand.handPose;
            }
            //Debug.Log("Nb left open : " + lastResponse.left_hand_pose + " , nb right open  : " + lastResponse.right_hand_pose);
            //Debug.Log("X : " + lastResponse.left_hand_x + ", Y " + lastResponse.left_hand_y);
        }
    }

}