using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace ai.hand
{
    [RequireComponent(typeof(RunHandLandmark))]
    public class HandTracking : MonoBehaviour
    {
        public float histThreshold = .8f;

        public int minHist = 15;

        public ai.hand.Hand currentMainLeftHand;
        public ai.hand.Hand currentMainRightHand;

        public void RunInference(ai.hand.HandCollection handCollection)
        {
            // Updated ids
            TrackHandCollectionID(handCollection);
            // Decide main hands
            UpdateMainHands(handCollection);
        }

        /// <summary>
        /// Check if current hand id has any hand in the history of the dictionary with that id
        /// </summary>
        /// <param name="handCollection">Hand collection with the dictionary</param>
        /// <param name="mainHand">Search if hand is form id</param>
        /// <returns>Returns hand only in the case the direction of the hand did not change</returns>
        public ai.hand.Hand CheckMainHand(ai.hand.HandCollection handCollection,ai.hand.Hand mainHand)
        {
            if (mainHand != null && handCollection.idHandDict.ContainsKey(mainHand.id))
            {
                ai.hand.Hand newHad = handCollection.idHandDict[mainHand.id][handCollection.idHandDict[mainHand.id].Count - 1];
                int newcurrentValue = GetLeftOrRightFromId(handCollection, mainHand.id, newHad.left_0_right_1);
                if (newcurrentValue == mainHand.left_0_right_1) // If average from same direction from previous hand, them return new hand
                    return AverageHandValues(newHad, mainHand);
                else
                    return null;
            }
            return null;
        }

        public ai.hand.Hand AverageHandValues(ai.hand.Hand currentHand, ai.hand.Hand previousHand)
        {
            currentHand.center = (7*previousHand.center+ 3*currentHand.center)/10;
            currentHand.cornerleftydown = (7*previousHand.cornerleftydown + 3 * currentHand.cornerleftydown)/ 10;
            currentHand.cornerleftup = (7 * previousHand.cornerleftup + 3 * currentHand.cornerleftup)/ 10;
            currentHand.cornerrightdown = (7 * previousHand.cornerrightdown + 3 * currentHand.cornerrightdown)/ 10;
            currentHand.cornerrightup = (7 * previousHand.cornerrightup + 3 * currentHand.cornerrightup)/ 10;
            currentHand.target_width = (7 * previousHand.target_width + 3 * currentHand.target_width)/ 10;
            currentHand.target_height = (7 * previousHand.target_height + 3 * currentHand.target_height)/ 10;

            for (int i = 0; i < currentHand.handNodes.Count; i++)
            {
                currentHand.handNodes[i] = (5*currentHand.handNodes[i] + 5*previousHand.handNodes[i]) / 10;
            }

            return currentHand;
        }

        public ai.hand.Hand AssignNewMainHand(ai.hand.HandCollection handCollection, ai.hand.Hand mainHand, int direction, int id_to_avoit)
        {
            foreach (int key in handCollection.idHandDict.Keys)
            {
                //Debug.Log(GetLeftOrRightFromId(handCollection, key, -1));
                Debug.Log(handCollection.idHandDict[key].Count);
                if (GetLeftOrRightFromId(handCollection, key, -1) == direction && handCollection.idHandDict[key].Count > minHist)
                {
                    // Avoid getting id from other hand
                    ai.hand.Hand newHand = handCollection.idHandDict[key][handCollection.idHandDict[key].Count - 1];
                    if (newHand.id != id_to_avoit)
                        return newHand;
                }
            }
            return null;
        }

        public void UpdateMainHands(ai.hand.HandCollection handCollection)
        {
            // If hand is from id that exists and if its still align to the good direction
            currentMainLeftHand = CheckMainHand(handCollection, currentMainLeftHand);
            currentMainRightHand = CheckMainHand(handCollection, currentMainRightHand);

            // Get ids to avoid for both hands
            int currentRightAvois = (currentMainLeftHand != null) ? currentMainLeftHand.id : -1;
            int currentLeftAvoids = (currentMainRightHand != null) ? currentMainRightHand.id : -1;

            // If hands are empty assin new hand
            if (currentMainLeftHand == null)
                currentMainLeftHand = AssignNewMainHand(handCollection, currentMainLeftHand,0, currentLeftAvoids);
            if (currentMainRightHand == null)
                currentMainRightHand = AssignNewMainHand(handCollection, currentMainRightHand,1, currentRightAvois);
        }

        public int GetLeftOrRightFromId(ai.hand.HandCollection handCollection, int id, int current)
        {
            int leftcount = (current==0)?  1:0;
            int rightcount = (current == 1) ? 1 : 0;
            foreach(ai.hand.Hand hand in handCollection.idHandDict[id])
            {
                if (hand.left_0_right_1 == 0)
                    leftcount += 1;
                else
                    rightcount += 1;
            }
            // Return higher number, if both are equal return the current frame value
            if (leftcount > rightcount)
                return 0;
            else if (rightcount > leftcount)
                return 1;
            else
                return current;
        }

        /// <summary>
        /// Based on history of previous frames' hands, decide which id to assign to each current frame's id
        /// </summary>
        /// <param name="handCollection"></param>
        public void TrackHandCollectionID(ai.hand.HandCollection handCollection)
        {
            List<int> keysToNotRemove = new List<int>();
            // For each hand
            foreach (ai.hand.Hand hand in handCollection.handsOnScreen)
            {
                float maxValue = 0f;
                int keyofmax = -1;
                // Check most overlaping previous hand out of all ids
                foreach (int key in handCollection.idHandDict.Keys)
                {
                    // For each past hand of this id
                    foreach (ai.hand.Hand pasthand in handCollection.idHandDict[key])
                    {
                        //Is overlapping by
                        float intersectionPercentage = OverlappingBy(hand, pasthand);
                        if (intersectionPercentage > maxValue)
                        {
                            maxValue = intersectionPercentage;
                            keyofmax = key;
                        }
                    }
                }

                // If key found and value past threshold
                if (keyofmax != 0 && maxValue > histThreshold)
                {
                    int itemsToRemove = (handCollection.idHandDict[keyofmax].Count + 1) - handCollection.maxHist;
                    if (itemsToRemove > 0)
                        handCollection.idHandDict[keyofmax].RemoveRange(0, Mathf.Min(itemsToRemove, handCollection.idHandDict[keyofmax].Count));
                    handCollection.idHandDict[keyofmax].Add(hand);
                    hand.id = keyofmax;
                    keysToNotRemove.Add(keyofmax);
                }
                // If key not found or value less than threshold
                else
                {
                    // Check for next available id
                    int availableId = GetAvailableKey(handCollection);
                    //Debug.Log("New available id created : " + availableId);
                    // Check for dictionary
                    handCollection.idHandDict[availableId] = new List<Hand>();
                    handCollection.idHandDict[availableId].Add(hand);
                    keysToNotRemove.Add(availableId);
                    hand.id = availableId;
                }

            }
            // Remove one previous hand for each id that was not key
            List<int> keyTOREMOVE = new List<int>();
            foreach (int key in handCollection.idHandDict.Keys)
            {
                // If key not identified
                if (!keysToNotRemove.Contains(key))
                {
                    handCollection.idHandDict[key].RemoveRange(0, Mathf.Min(1, handCollection.idHandDict[key].Count));
                    // Remove key if it does not have any hand anymore
                    if (handCollection.idHandDict[key].Count == 0)
                        keyTOREMOVE.Add(key);
                }
            }
            foreach(int key in keyTOREMOVE)
                handCollection.idHandDict.Remove(key);
        }

        private int GetAvailableKey(ai.hand.HandCollection handCollection)
        {
            List<int> keyList = new List<int>(handCollection.idHandDict.Keys);
            keyList.Add(0); // Making sure if list is empty than at least returns 1 at the end
            int max = keyList.Max();
            // if lower than max, then no problemo
            if (max < handCollection.maxId)
                return max + 1;
            else // if higher than max
            {
                // If keylist is at max, return the first position
                if(keyList.Count >= handCollection.maxId)
                {
                    return 1;
                } else
                {
                    // Check for available id
                    for(int i=1; i < keyList.Count+1; i++)
                    {
                        if (!keyList.Contains(i))
                            return i;
                    }
                }
            }
            return 1; // Worst case cenario

        }



        /// <summary>
        /// Returns the lowest intersection area divided by hand area rate between two hands
        /// </summary>
        /// <param name="hand1"></param>
        /// <param name="hand2"></param>
        /// <returns></returns>
        public static float OverlappingBy(ai.hand.Hand hand1, ai.hand.Hand hand2)
        {
            // get overlapping coordinages
            int x1 = Mathf.Max(hand1.cornerleftup.x, hand2.cornerleftup.x);
            int x2 = Mathf.Min(hand1.cornerrightup.x, hand2.cornerrightup.x);
            int y1 = Mathf.Max(hand1.cornerleftup.y, hand2.cornerleftup.y);
            int y2 = Mathf.Min(hand1.cornerleftydown.y, hand2.cornerleftydown.y);
            if (x1 > x2 || y1 > y2)
                return 0f;
            float intersectionArea = (x2 - x1) * (y2 - y1);
            float hand1area = (hand1.cornerrightup.x-hand1.cornerleftup.x) * (hand1.cornerleftydown.y- hand1.cornerleftup.y);
            float hand2area = (hand2.cornerrightup.x-hand2.cornerleftup.x) * (hand2.cornerleftydown.y- hand2.cornerleftup.y);
            return Mathf.Max(intersectionArea/hand1area, intersectionArea/hand2area);
        }

        public static float IOU(ai.hand.Hand hand1, ai.hand.Hand hand2)
        {
            // get overlapping coordinages
            int x1 = Mathf.Max(hand1.cornerleftup.x, hand2.cornerleftup.x);
            int x2 = Mathf.Min(hand1.cornerrightup.x, hand2.cornerrightup.x);
            int y1 = Mathf.Max(hand1.cornerleftup.y, hand2.cornerleftup.y);
            int y2 = Mathf.Min(hand1.cornerleftydown.y, hand2.cornerleftydown.y);
            if (x1 > x2 || y1 > y2)
                return 0f;
            float intersectionArea = (x2 - x1) * (y2 - y1);
            float hand1area = (hand1.cornerrightup.x - hand1.cornerleftup.x) * (hand1.cornerleftydown.y - hand1.cornerleftup.y);
            float hand2area = (hand2.cornerrightup.x - hand2.cornerleftup.x) * (hand2.cornerleftydown.y - hand2.cornerleftup.y);
            return intersectionArea/ (hand1area + hand2area - intersectionArea);
        }

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            //Debug.Log("Right hand id : " + currentMainRightHand?.id + ", Left hand id : " + currentMainLeftHand?.id);
        }
    }

}