using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Sentis;
using UnityEngine.Video;
using UnityEngine.UI;
using Lays = Unity.Sentis.Layers;
using System;


/*
 *                   Hand Landmarks Inference
 *                   ========================
 *                   
 * Basic inference script for mediapose hand landmarks
 * 
 * Put this script on the Main Camera
 * Put hand_landmark_sparse_Nx3x224x224.sentis in the Assets/StreamingAssets folder
 * Create a RawImage of in the scene
 * Put a link to that image in previewUI
 * Put a video in Assets/StreamingAssets folder and put the name of it int videoName
 * Or put a test image in inputImage
 * Set inputType to appropriate input
 */
public class RunHandLandmark : MonoBehaviour
{

    // HandLandMarkBones
    private readonly List<int> handlandmarkBones = new List<int>(new int[] {
    0, 1,1, 2,2, 3,3, 4,
        0, 5,5, 6,6, 7,7, 8,
        5, 9,9, 10,10, 11,11, 12,
        9, 13,13, 14,14, 15,15, 16,
        13, 17,17, 18,18, 19,19, 20,0, 17
    });

    const BackendType backend = BackendType.GPUCompute;
    public const int markerWidth = 5; // get it from other script

    //Holds array of colors to draw landmarks
    Color32[] markerPixels;

    IWorker worker;

    //Size of input image to neural network (196)
    const int size = 224;

    Ops ops;

    Model model;
    public ModelAsset modelAsset;

    // Model parameters
    public float hand_scr_threshold = 0.5f;

    bool closing = false;
    public bool showlines = false;

    #region starting
    // Start is called before the first frame update
    void Start()
    {
        
        //(Note: if using a webcam on mobile get permissions here first)
        SetupMarkers(Color.green);
        SetupModel();
        SetupPalm();
        SetupEngine();
    }

    private void SetupPalm()
    {
    }

    void SetupMarkers(Color color)
    {
        markerPixels = new Color32[markerWidth * markerWidth];
        for (int n = 0; n < markerWidth * markerWidth; n++)
        {
            markerPixels[n] = color;
        }
        int center = markerWidth / 2;
        markerPixels[center * markerWidth + center] = Color.black;
    }

    public void SetupEngine()
    {
        worker = WorkerFactory.CreateWorker(backend, model);
    }

    void SetupModel()
    {
        model = ModelLoader.Load(Application.streamingAssetsPath + "/hand_landmark_sparse_Nx3x224x224.sentis");
        //model = ModelLoader.Load(modelAsset);

    }
    #endregion


    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            closing = true;
            Application.Quit();
        }
    }

    #region lateupdate
    void LateUpdate()
    {

    }

    

    public void RunInference(ai.hand.PalmExtractor palmDetection)
    {
        // For each palm
        foreach (ai.hand.Hand palm in palmDetection.handCollection.handsOnScreen)
        {
            Texture2D onehand = HandUtils.toTexture2D(palmDetection.targetTexture,palm.cornerleftup.x, palm.cornerrightup.x,
                palmDetection.targetTexture.height + 1 -palm.cornerleftydown.y, palmDetection.targetTexture.height + 1 - palm.cornerleftup.y);
            var transform = new TextureTransform();
            transform.SetDimensions(size, size, 3);
            transform.SetTensorLayout(0, 1, 2, 3);
            using var image0 = TextureConverter.ToTensor(onehand, transform);
            worker.Execute(image0);
            using var landmarks = worker.PeekOutput("xyz_x21") as TensorFloat;
            using var hand_score = worker.PeekOutput("hand_score") as TensorFloat;
            using var leftright = worker.PeekOutput("lefthand_0_or_righthand_1") as TensorFloat;
            

            //This gives the condifidence:
            //using var confidence = worker.PeekOutput("conv2d_31") as TensorFloat;

            float scaleX = onehand.width * 1f / size;
            float scaleY = onehand.height * 1f / size;

            landmarks.CompleteOperationsAndDownload();
            hand_score.CompleteOperationsAndDownload();
            leftright.CompleteOperationsAndDownload();
            palm.left_0_right_1 = (int)leftright[0, 0];
            //Debug.Log(palm.left_0_right_1);

            // TO remove
            hand_scr_threshold = 0;
            DrawLandmarks(landmarks, scaleX, scaleY, palm, palmDetection);
        }
    }

    void DrawLandmarks(TensorFloat landmarks, float scaleX, float scaleY, ai.hand.Hand palm, ai.hand.PalmExtractor palmDetection)
    {
        // palm_scr_threshold
        int numLandmarks = landmarks.shape[1]/3; //21 palm detection landmarks
        //Debug.Log(numLandmarks);
        RenderTexture.active = palmDetection.targetTexture;
        //palmDetection.canvasTexture.ReadPixels(new Rect(0, 0, palmDetection.targetTexture.width, palmDetection.targetTexture.height), 0, 0);
        List<Vector3Int> listOfNodes = new List<Vector3Int>(); 
        for (int n = 0; n < numLandmarks; n++)
        {
            int px = (int)(landmarks[0, n * 3 + 0] * scaleX) - (markerWidth - 1) / 2;
            int py = (int)(landmarks[0, n * 3 + 1] * scaleY) - (markerWidth - 1) / 2;
            int pz = (int)(landmarks[0, n * 3 + 2] * scaleX);
            int destX = Mathf.Clamp(px + palm.cornerleftup.x, 0, palmDetection.targetTexture.width - 1 - markerWidth);
            int destY = Mathf.Clamp(palmDetection.targetTexture.height - 1 - py -(palmDetection.targetTexture.height + 1 - palm.cornerleftydown.y), 0, palmDetection.targetTexture.height - 1 - markerWidth);
            listOfNodes.Add(new Vector3Int(destX, destY, pz));
            if(showlines)
            palmDetection.canvasTexture.SetPixels32(destX, destY, markerWidth, markerWidth, markerPixels);
        }
        if (showlines)
        {
            for (int i = 0; i < handlandmarkBones.Count / 2; i++)
            {
                HandUtils.DrawLine(palmDetection.canvasTexture,
                    listOfNodes[handlandmarkBones[2 * i + 0]].x, listOfNodes[handlandmarkBones[2 * i + 0]].y + (markerWidth / 2), listOfNodes[handlandmarkBones[2 * i + 1]].x,
                    listOfNodes[handlandmarkBones[2 * i + 1]].y + (markerWidth / 2), Color.green);
            }
        }
        // Put nodes on hand/palm object
        palm.handNodes = listOfNodes;
        palmDetection.canvasTexture.Apply();
        Graphics.Blit(palmDetection.canvasTexture, palmDetection.targetTexture);
        RenderTexture.active = null;
    }

    #endregion


    void CleanUp()
    {
        closing = true;
        ops?.Dispose();
        
        worker?.Dispose();
        worker = null;
    }

    void OnDestroy()
    {
        CleanUp();
    }
}
