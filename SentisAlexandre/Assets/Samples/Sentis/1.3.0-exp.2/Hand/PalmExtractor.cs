using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Sentis;
using UnityEngine.Video;
using UnityEngine.UI;
using Lays = Unity.Sentis.Layers;
using System.Globalization;
using System;


namespace ai.hand
{
    public abstract class PalmExtractor : MonoBehaviour
    {
        protected ai.server.UnityHandServer unityServer;
        protected RunHandLandmark handlandmark;
        protected ai.hand.HandTracking handTracking;
        protected ai.hand.HandPoseEstimation handPoseEstimation;
        protected List<Dictionary<string, object>> anchors;

        public ai.hand.HandCollection handCollection;




        //Drag a link to a raw image here:
        public RawImage previewUI = null;

        public string videoName = "chatting.mp4";

        // Image to put into neural network
        public Texture2D inputImage;

        public InputType inputType = InputType.Video;

        //Resolution of displayed image
        protected Vector2Int resolution = new Vector2Int(640, 640);
        protected WebCamTexture webcam;
        protected VideoPlayer video;

        protected const BackendType backend = BackendType.GPUCompute;

        public RenderTexture targetTexture; // get it from other script

        public enum InputType { Image, Video, Webcam };

        public const int markerWidth = 5; // get it from other script

        //Holds array of colors to draw landmarks
        protected Color32[] markerPixels;

        protected IWorker worker;


        protected Ops ops;

        protected Model model;
        public ModelAsset modelAsset;

        // Model parameters
        public float palm_scr_threshold = 0.5f;
        public float overlap_threshold = .8f;
        public float increaseXBoxSizeBy = 2f;
        public float increaseYBoxSizeBy = 2f;
        public float offsetXBy = 0f;
        public float offsetYBy = .5f;

        //webcam device name:
        protected const string deviceName = "";

        protected bool closing = false;
        public bool showlines = false;

        public Texture2D canvasTexture; // Get it from other script

        public bool printInPreview = false;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }

}