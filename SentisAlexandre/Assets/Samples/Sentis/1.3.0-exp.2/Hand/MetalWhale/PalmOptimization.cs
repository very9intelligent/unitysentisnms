using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Sentis;
using UnityEngine.Video;
using UnityEngine.UI;
using Lays = Unity.Sentis.Layers;
using System.Globalization;
using System;

/*
 * TO DO
 * - Tracking : only track as right and left if it has more than threshold frames
 * */

public class PalmOptimization : ai.hand.PalmExtractor
{

    //Size of input image to neural network (196)
    protected const int size = 256;

    #region starting
    // Start is called before the first frame update
    void Start()
    {

        //(Note: if using a webcam on mobile get permissions here first)
        SetupTextures();
        SetupMarkers(Color.red);
        SetupInput();
        SetupModel();
        SetupEngine();

        // Create palm tracker
        handlandmark = GetComponent<RunHandLandmark>();
        handCollection = GetComponent<ai.hand.HandCollection>();
        handTracking = GetComponent<ai.hand.HandTracking>();
        handPoseEstimation = GetComponent<ai.hand.HandPoseEstimation>();
        unityServer = GetComponent<ai.server.UnityHandServer>();
        anchors = CSVReader.Read("anchors_2944");
    }

    public void SetupEngine()
    {
        worker = WorkerFactory.CreateWorker(backend, model);
        ops = WorkerFactory.CreateOps(backend);
    }

    void SetupModel()
    {
        model = ModelLoader.Load(Application.streamingAssetsPath + "/palm_anchors_2944.sentis");
        //model = ModelLoader.Load(modelAsset);

    }

    void SetupMarkers(Color color)
    {
        markerPixels = new Color32[markerWidth * markerWidth];
        for (int n = 0; n < markerWidth * markerWidth; n++)
        {
            markerPixels[n] = color;
        }
        int center = markerWidth / 2;
        markerPixels[center * markerWidth + center] = Color.black;
    }

    void SetupInput()
    {
        switch (inputType)
        {
            case InputType.Webcam:
                {
                    webcam = new WebCamTexture(deviceName, resolution.x, resolution.y);
                    webcam.requestedFPS = 30;
                    webcam.Play();
                    break;
                }
            case InputType.Video:
                {
                    video = gameObject.AddComponent<VideoPlayer>();//new VideoPlayer();
                    video.renderMode = VideoRenderMode.APIOnly;
                    video.source = VideoSource.Url;
                    video.url = Application.streamingAssetsPath + "/" + videoName;
                    video.isLooping = true;
                    video.Play();
                    break;
                }
            default:
                {
                    Graphics.Blit(inputImage, targetTexture);
                }
                break;
        }
    }

    void SetupTextures()
    {
        //To display the get and display the original image:
        targetTexture = new RenderTexture(resolution.x, resolution.y, 0);

        //Used for drawing the markers:
        canvasTexture = new Texture2D(targetTexture.width, targetTexture.height);
        if (previewUI != null )
            previewUI.texture = targetTexture;
    }

    #endregion

    #region updating
    void GetImageFromSource()
    {
        if (inputType == InputType.Webcam)
        {
            // Format video input
            if (!webcam.didUpdateThisFrame) return;

            var aspect1 = (float)webcam.width / webcam.height;
            var aspect2 = (float)resolution.x / resolution.y;
            var gap = aspect2 / aspect1;

            var vflip = webcam.videoVerticallyMirrored;
            var scale = new Vector2(gap, vflip ? -1 : 1);
            var offset = new Vector2((1 - gap) / 2, vflip ? 1 : 0);

            Graphics.Blit(webcam, targetTexture, new Vector2(1, 1), new Vector2(0, 0));
        }
        if (inputType == InputType.Video)
        {
            var aspect1 = (float)video.width / video.height;
            var aspect2 = (float)resolution.x / resolution.y;
            var gap = aspect2 / aspect1;

            var vflip = false;
            var scale = new Vector2(gap, vflip ? -1 : 1);
            var offset = new Vector2((1 - gap) / 2, vflip ? 1 : 0);
            Graphics.Blit(video.texture, targetTexture, scale, offset);
        }
        if (inputType == InputType.Image)
        {
            Graphics.Blit(inputImage, targetTexture);
        }
    }

    // Update is called once per frame
    void Update()
    {

        GetImageFromSource();

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            closing = true;
            Application.Quit();
        }

        if (Input.GetKeyDown(KeyCode.P))
        {
            previewUI.enabled = !previewUI.enabled;
        }
        //Debug.Log(1 / (Time.deltaTime));
    }
    #endregion

    #region LateUpdating
    void LateUpdate()
    {
        
        if (!closing)
        {
            RunInference(targetTexture);
            if (handlandmark != null && handlandmark.enabled)
                handlandmark?.RunInference(this);
            if (handTracking != null && handTracking.enabled)
                handTracking.RunInference(handCollection);
            if (handPoseEstimation != null && handPoseEstimation.enabled)
                handPoseEstimation.RunInference(handCollection, handTracking);
            if (unityServer != null && unityServer.enabled)
                unityServer.UpdateServerResponse();
        }
    }

    void RunInference(Texture source)
    {
        var transform = new TextureTransform();
        transform.SetDimensions(size, size, 3);
        transform.SetTensorLayout(0, 3, 1,2);
        
        using var image0 = TextureConverter.ToTensor(targetTexture, transform);
        Debug.Log(image0);
        using var imagetemp = ops.Mul(2f, image0);
        using var image = ops.Add(imagetemp, -1f);
        worker.Execute(image);

        using var landmarks = worker.PeekOutput("regressors/concat") as TensorFloat;
        using var probabilities = worker.PeekOutput("classificators/concat") as TensorFloat;

        float scaleX = targetTexture.width * 1f / size;
        float scaleY = targetTexture.height * 1f / size;

        landmarks.CompleteOperationsAndDownload();
        probabilities.CompleteOperationsAndDownload();
        DrawLandmarks(landmarks, probabilities, scaleX, scaleY, source);
    }

    float SoftPlus(float x)
    {
        return Mathf.Log(1f + Mathf.Exp(x));
    }

    float Sigma(float x)
    {
        return 1f / (1f + Mathf.Exp(-x));
    }

    float NormalizeRadians(float angle)
    {
        return angle - 2 * Mathf.PI * Mathf.Floor((angle + Mathf.PI) / (2 * Mathf.PI));
    }

    void DrawLandmarks(TensorFloat landmarks, TensorFloat probabilities, float scaleX, float scaleY, Texture source)
    {
        // palm_scr_threshold
        int numLandmarks = landmarks.shape[1]; //2016 anchor points

        if(previewUI != null) { 
            RenderTexture.active = targetTexture;
            canvasTexture.ReadPixels(new Rect(0, 0, targetTexture.width, targetTexture.height), 0, 0);
        }

        handCollection.handsOnScreen = new List<ai.hand.Hand>();
        for (int n = 0; n < numLandmarks; n++)
        {
            // For each box
            float prob = SoftPlus(probabilities[0, n, 0]);
            if (prob > palm_scr_threshold)
            {

                float dx = landmarks[0, n, 0];
                float dy = landmarks[0, n, 1];
                float w = landmarks[0, n, 2];
                float h = landmarks[0, n, 3];
                Dictionary<string, object> anchor = anchors[n];
                // First, get conrer maximal points
                float anchor_x = float.Parse(anchor["x"] + "", CultureInfo.InvariantCulture);
                float anchor_y = float.Parse(anchor["y"] + "", CultureInfo.InvariantCulture);

                AddHand(anchor_x, anchor_y, dx,dy,w,h, scaleX, scaleY, prob);
                
            }

        }

        if (printInPreview)
        {
            DrawPalmCenter(markerWidth);
            DrawHands(markerWidth);
        }
        if (previewUI != null)
        {
            canvasTexture.Apply();
            Graphics.Blit(canvasTexture, targetTexture);
            RenderTexture.active = null;
        }

    }

    private void AddHand(float anchor_x,float  anchor_y, float dx, float dy, float w, float h, float scaleX, float scaleY, float prob)
    {
        // Fist get all maximal points from corners
        int xmin = (int)(anchor_x * targetTexture.width) + (int)(((dx - w * 1f / 2) * scaleX) * increaseXBoxSizeBy) + (int)((dx + w / 2) *scaleX* increaseXBoxSizeBy * offsetXBy);
        int ymin = (int)(anchor_y * targetTexture.height) + (int)(((dy - h * 1f / 2) * scaleY) * increaseYBoxSizeBy) + (int)((dy - h / 2)*scaleY * increaseYBoxSizeBy * offsetYBy);
        int xmax = (int)(anchor_x * targetTexture.width) + (int)(((dx + w * 1f / 2) * scaleX) * increaseXBoxSizeBy) + (int)((dx + w / 2) * scaleX* increaseXBoxSizeBy*offsetXBy);
        int ymax = (int)(anchor_y * targetTexture.height) + (int)(((dy + h * 1f / 2) * scaleY) * increaseYBoxSizeBy) + (int)((dy - h / 2) * scaleY* increaseYBoxSizeBy* offsetYBy);
        int width = targetTexture.width;
        int height = targetTexture.height;
        xmin = Mathf.Max(xmin, 0);
        xmax = Mathf.Min(xmax, width);
        ymin = Mathf.Max(ymin, 0);
        ymax = Mathf.Min(ymax, height);
        // Creating a PalmSquare for this hand
        ai.hand.Hand currentHand = new ai.hand.Hand();
        currentHand.cornerleftydown = GetScreenPointFromPoint(xmin - (markerWidth / 2), ymin - (markerWidth / 2), width, height, markerWidth);
        currentHand.cornerleftup = GetScreenPointFromPoint(xmin - (markerWidth / 2), ymax - (markerWidth / 2), width, height, markerWidth);
        currentHand.cornerrightdown = GetScreenPointFromPoint(xmax - (markerWidth / 2), ymin, width - (markerWidth / 2), height, markerWidth);
        currentHand.cornerrightup = GetScreenPointFromPoint(xmax - (markerWidth / 2), ymax - (markerWidth / 2), width, height, markerWidth);
        currentHand.center = GetScreenPointFromPercentage(anchor_x, anchor_y, targetTexture.width, targetTexture.height, markerWidth);
        currentHand.target_width = width;
        currentHand.target_height = height;
        currentHand.prob = prob;

        // Lift of hands to remove
        List<ai.hand.Hand> handsToRemove = new List<ai.hand.Hand>();
        // For each hand in hands on screen do
        foreach (ai.hand.Hand collectionHand in handCollection.handsOnScreen)
        {
            // if overlaping area higher than threshold then remove
            float overlapingby = ai.hand.HandTracking.OverlappingBy(currentHand, collectionHand);
            if(overlapingby > overlap_threshold)
                handsToRemove.Add(collectionHand);
        }
        // Add new hand
        handsToRemove.Add(currentHand);
        ai.hand.Hand outHand = CriteriaMaxSize(handsToRemove);
        // Remove rest
        foreach (ai.hand.Hand hand in handsToRemove)
        {
            if(handCollection.handsOnScreen.Contains(hand))
                handCollection.handsOnScreen.Remove(hand);
        }
        handCollection.handsOnScreen.Add(outHand);
    }

    public ai.hand.Hand CriteriaProb(List<ai.hand.Hand> handsToRemove)
    {
        float highest_prob = 0f;
        ai.hand.Hand outHand = null;
        foreach (ai.hand.Hand hand in handsToRemove)
        {
            if (highest_prob < hand.prob)
            {
                highest_prob = hand.prob;
                outHand = hand;
            }
        }
        return outHand;
    }


    public ai.hand.Hand CriteriaMaxSize(List<ai.hand.Hand> handsToRemove)
    {
        float biggestArea = 0f;
        ai.hand.Hand outHand = null;
        foreach (ai.hand.Hand hand in handsToRemove)
        {
            float hand1area = (hand.cornerrightup.x - hand.cornerleftup.x) * (hand.cornerleftydown.y - hand.cornerleftup.y);
            if (biggestArea < hand1area)
            {
                biggestArea = hand1area;
                outHand = hand;
            }
        }
        return outHand;
    }

    private void DrawPalmCenter(int markerWidth)
    {
        foreach (ai.hand.Hand palmSquare in handCollection.handsOnScreen)
        {
            canvasTexture.SetPixels32(palmSquare.center.x, palmSquare.center.y, markerWidth, markerWidth, markerPixels);
        }
    }


    private void DrawHands(int markerWidth)
    {
        // Creating a PalmSquare for this hand
        foreach(ai.hand.Hand palmSquare in handCollection.handsOnScreen) { 
            // Painting into canvas
            if (printInPreview)
            {
                canvasTexture.SetPixels32(palmSquare.cornerleftydown.x, palmSquare.cornerleftydown.y, markerWidth, markerWidth, markerPixels);
                canvasTexture.SetPixels32(palmSquare.cornerleftup.x, palmSquare.cornerleftup.y, markerWidth, markerWidth, markerPixels);
                canvasTexture.SetPixels32(palmSquare.cornerrightdown.x, palmSquare.cornerrightdown.y, markerWidth, markerWidth, markerPixels);
                canvasTexture.SetPixels32(palmSquare.cornerrightup.x, palmSquare.cornerrightup.y, markerWidth, markerWidth, markerPixels);
            }

            if (showlines && printInPreview)
            {
                HandUtils.DrawLine(canvasTexture, palmSquare.cornerleftup.x, palmSquare.cornerrightdown.y + (markerWidth / 2), palmSquare.cornerrightup.x, palmSquare.cornerrightdown.y + (markerWidth / 2), Color.red);
                HandUtils.DrawLine(canvasTexture, palmSquare.cornerleftup.x, palmSquare.cornerrightup.y + (markerWidth / 2), palmSquare.cornerrightup.x, palmSquare.cornerrightup.y + (markerWidth / 2), Color.red);
                HandUtils.DrawLine(canvasTexture, palmSquare.cornerleftup.x, palmSquare.cornerrightdown.y + (markerWidth / 2), palmSquare.cornerleftup.x, palmSquare.cornerrightup.y + (markerWidth / 2), Color.red);
                HandUtils.DrawLine(canvasTexture, palmSquare.cornerrightup.x, palmSquare.cornerrightdown.y + (markerWidth / 2), palmSquare.cornerrightup.x, palmSquare.cornerrightup.y + (markerWidth / 2), Color.red);
            }
        }
    }



    private Vector2Int GetScreenPointFromPoint(int xpoint, int ypoint, int width, int height, int markerWidth)
    {
        int destX = Mathf.Clamp(xpoint - (markerWidth - 1) / 2, 0, width - 1 - markerWidth);
        int destY = Mathf.Clamp(height - 1 - (ypoint - (markerWidth - 1) / 2), 0, height - 1 - markerWidth);
        return new Vector2Int(destX, destY);
    }

    private Vector2Int GetScreenPointFromPercentage(float sqn_rr_center_x, float sqn_rr_center_y, int width, int height, int markerWidth)
    {
        int destX = Mathf.Clamp((int)(sqn_rr_center_x * width), (markerWidth/2), width - 1 - markerWidth / 2) ;
        int destY = Mathf.Clamp(height - 1 - (int)(sqn_rr_center_y * height), (markerWidth / 2), height - 1 - markerWidth / 2);
        return new Vector2Int(destX, destY);
    }


    #endregion


    void CleanUp()
    {
        closing = true;
        ops?.Dispose();
        
        if (webcam) Destroy(webcam);
        if (video) Destroy(video);
        RenderTexture.active = null;
        targetTexture.Release();
        worker?.Dispose();
        worker = null;
    }

    void OnDestroy()
    {
        CleanUp();
    }
}
