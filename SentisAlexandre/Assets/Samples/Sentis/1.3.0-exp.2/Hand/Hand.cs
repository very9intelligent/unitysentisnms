using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ai.hand
{
    public class Hand
    {

        public enum HandPose
        {
            OPEN_HAND,
            COUNTING_ONE,
            COUNTING_TWO,
            COUNTING_THREE,
            CLOSED_HAND,
        }

        // Palm Detection
        public Vector2Int center;
        public Vector2Int cornerleftydown;
        public Vector2Int cornerleftup;
        public Vector2Int cornerrightdown;
        public Vector2Int cornerrightup;

        // Original image
        public int target_width;
        public int target_height;

        public float prob;

        // Hand Skeleton
        public List<Vector3Int> handNodes;
        public int left_0_right_1;
        public int id;

        // This hand pose
        public HandPose handPose = HandPose.OPEN_HAND;

        public void CountingFingersToPose(int count)
        {
            switch (count)
            {
                case 0:
                    handPose = HandPose.CLOSED_HAND;
                    break;
                case 1:
                    handPose = HandPose.COUNTING_ONE;
                    break;
                case 2:
                    handPose = HandPose.COUNTING_TWO;
                    break;
                case 3:
                    handPose = HandPose.COUNTING_THREE;
                    break;
                default:
                    handPose = HandPose.OPEN_HAND;
                    break;
            }
                
        }

        public Hand()
        {
            handNodes = new List<Vector3Int>();
        }

        public Hand Clone()
        {
            Hand handCopy = new Hand();
            handCopy.center = this.center;
            handCopy.cornerleftydown = this.cornerleftydown;
            handCopy.cornerleftup = this.cornerleftup;
            handCopy.cornerrightdown = this.cornerrightdown;
            handCopy.cornerrightup = this.cornerrightup;
            handCopy.handNodes = new List<Vector3Int>(this.handNodes);
            handCopy.left_0_right_1 = this.left_0_right_1;
            handCopy.id = this.id;
            handCopy.target_width = this.target_width;
            handCopy.target_height = this.target_height;
            handCopy.handPose = this.handPose;
            return handCopy;
        }

    }

}