using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace utils.manager
{
    // Controls the manager that goes trough each scene
    public class AllScenesManager : MonoBehaviour
    {
        void Awake()
        {
            DontDestroyOnLoad(this.gameObject);

            GameObject[] objs = GameObject.FindGameObjectsWithTag("AllScenesManager");

            if (objs.Length > 1)
            {
                foreach(GameObject obj in objs) { 
                    if(this.gameObject != obj)
                        Destroy(obj);
                }
            }

            
        }
    }

}