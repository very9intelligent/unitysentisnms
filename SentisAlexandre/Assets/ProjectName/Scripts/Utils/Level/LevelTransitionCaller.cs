﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace utils.level
{

    public class LevelTransitionCaller : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }


        public void ChangeLevelAssync(string levelToLoad)
        {
            GameObject.FindGameObjectWithTag("TransitionController")
                .GetComponent<utils.level.LevelTransitionController>().FullTransitionTo(levelToLoad, true);
        }

        public void ChangeLevelSync(string levelToLoad)
        {
            GameObject.FindGameObjectWithTag("TransitionController")
                .GetComponent<utils.level.LevelTransitionController>().SyncLoad(levelToLoad);
        }

    }

}